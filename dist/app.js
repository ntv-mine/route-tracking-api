'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _bodyParser = require('body-parser');

var _bodyParser2 = _interopRequireDefault(_bodyParser);

var _socket = require('socket.io');

var _socket2 = _interopRequireDefault(_socket);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _config = require('config');

var _config2 = _interopRequireDefault(_config);

var _routes = require('./routes');

var _routes2 = _interopRequireDefault(_routes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const app = (0, _express2.default)();

app.disable('x-powered-by'); // disable x-powered-by
app.set('port', _config2.default.get('app.port'));

app.use(_bodyParser2.default.json());
app.use(_bodyParser2.default.urlencoded({ extended: false }));

app.use('/api', _routes2.default);

// Index route
app.use('/', (req, res) => {
  res.sendFile(_path2.default.join(__dirname, '/../src/index.html'));
});

// catch 404 and forward to error handler
app.use((req, res, next) => {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use((err, req, res, next) => {
  console.log('error handler', err.message);
  res.json({
    success: false,
    error: err.message
  });
});

const server = app.listen(app.get('port'), () => {
  console.log(`Environment => ${_config2.default.util.getEnv('NODE_ENV')}`);
});

const io = (0, _socket2.default)(server);
io.on('connection', socket => {
  console.log('Client connected');
  socket.on('disconnect', () => console.log('Client disconnected'));
});

setInterval(() => io.emit('time', new Date().toTimeString()), 1000);

exports.default = app;
//# sourceMappingURL=app.js.map