'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _config = require('config');

var _config2 = _interopRequireDefault(_config);

var _pg = require('pg');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

class Controller {
  constructor() {
    this.pool = new _pg.Pool({
      connectionString: _config2.default.get('db.connectionString')
    });
  }
}

exports.default = Controller;
//# sourceMappingURL=controller.js.map