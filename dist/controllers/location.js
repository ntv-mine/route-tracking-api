'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _controller = require('./controller');

var _controller2 = _interopRequireDefault(_controller);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

class LocationController extends _controller2.default {
  constructor() {
    super();
  }

  query(req, res, next) {
    return res.json({
      success: true,
      data: req.query
    });
  }

  create(req, res, next) {
    console.log('New Location: ', JSON.stringify(req.body, null, 2));

    res.json({
      success: true,
      data: req.body
    });
  }
}
exports.default = LocationController;
//# sourceMappingURL=location.js.map