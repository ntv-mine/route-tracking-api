'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require('express');

var _location = require('../controllers/location');

var _location2 = _interopRequireDefault(_location);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const router = (0, _express.Router)();

// Location

const locationCtrl = new _location2.default();

router.route('/location').get(locationCtrl.query).post(locationCtrl.create);

exports.default = router;
//# sourceMappingURL=index.js.map