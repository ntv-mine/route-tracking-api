import express from 'express';
import bodyParser from 'body-parser';
import socketIO from 'socket.io';
import path from 'path';
import config from 'config';

import routes from './routes';

const app = express();

app.disable('x-powered-by'); // disable x-powered-by
app.set('port', config.get('app.port'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use('/api', routes);

// Index route
app.use('/', (req, res) => {
  res.sendFile(path.join(__dirname, '/../src/index.html'));
});

// catch 404 and forward to error handler
app.use((req, res, next) => {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use((err, req, res, next) => {
  console.log('error handler', err.message);
  res.json({
    success: false,
    error: err.message,
  });
});

const server = app.listen(app.get('port'), () => {
  console.log(`Environment => ${config.util.getEnv('NODE_ENV')}`);
});

const io = socketIO(server);
io.on('connection', (socket) => {
  console.log('Client connected');
  socket.on('disconnect', () => console.log('Client disconnected'));
});

app.set('io', io);

export default app;
