import config from 'config';
import { Pool } from 'pg';

class Controller {
  constructor() {
    this.pool = new Pool({
      connectionString: config.get('db.connectionString'),
    });
  }
}

export default Controller;
