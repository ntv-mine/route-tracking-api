import Controller from './controller';

export default class LocationController extends Controller {
  constructor() {
    super();

    // console.log('test', iox);
  }

  query(req, res, next) {
    return res.json({
      success: true,
      data: req.query,
    });
  }

  create(req, res, next) {
    console.log('New Location: ', req.body);

    req.app.get('io').emit('location', { data: req.body });

    res.json({
      success: true,
      data: req.body,
    });
  }
}
