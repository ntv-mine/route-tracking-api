import { Router } from 'express';
const router = Router();

// Location
import LocationController from '../controllers/location';
const locationCtrl = new LocationController();

router.route('/location')
  .get(locationCtrl.query)
  .post(locationCtrl.create);

export default router;
